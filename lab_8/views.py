from django.shortcuts import render


response = {}
def index(request):
    html = 'lab_8/lab_8.html'
    response = {'author': "Bram Sedana"}
    return render(request, html, response)
